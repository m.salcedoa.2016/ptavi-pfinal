#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import socket
import sys
import time
import json
import os.path as path
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from uaclient import write_log

dicc = {}
user_origin = {}


class XMLHandlerProxy(ContentHandler):
    """
    Clase para manejar los XML
    """

    def __init__(self):
        """
        Constructor. Inicializamos la lista.
        """
        self.tags = {
            "server": ['name', 'ip', 'puerto'],
            "database": ['path', 'passwdpath'],
            "log": ['path']
        }
        self.tag_list = []

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name in self.tags:
            # Creamos una lista
            tag_list = {}
            tag_list["tag"] = name
            for attribute in self.tags[name]:
                tag_list[attribute] = attrs.get(attribute, "")
            self.tag_list.append(tag_list)

    def get_tags(self):
        return self.tag_list


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """handle method of the server class"""

        # Bucle
        while 1:
            # Sacamos la linea del mensaje recibido
            line = self.rfile.read()
            line = line.decode('utf-8')
            information = line.split('\r\n')
            if line:
                if line[0:8] == "REGISTER":
                    # Obtenemos el email del nuevo cliente
                    username = information[0].split()[-2]
                    email = username.split(":")[-2]
                    # Obtenemos el puerto del cliente
                    port_client = username.split(":")[-1]
                    # Obtenemos el tiempo de expiración del cliente
                    expires_time = int(information[1].split()[-1])
                    # Obtenemos los segundos desde le 1-1-1970
                    time_register = time.strftime('%s',
                                                  time.localtime(time.time()))
                    # Añadimos el usuario en la lista.
                    user_list[email] = [self.client_address[0],
                                        port_client,
                                        time_register,
                                        expires_time]
                    # Comprobamos si hay fichero y registramos en el fichero
                    self.json2register()
                    # Guardamos en el fichero la lista.
                    self.register2json()
                    write_log(line, "Received from", self.client_address[0],
                              port_client, log_path)
                    # Enviamos un 200 OK para confirmar
                    self.wfile.write(bytes("SIP/2.0 200 OK\r\n", 'utf-8'))
                if line[0:6] == "INVITE":
                    # Sacamos el email al que va el INVITE
                    email_destination = information[0].split()[-2]
                    email_destination = email_destination.split(":")[-1]
                    # Sacamos el email origen
                    email_origen = information[5].split()[-2]
                    email_origen = email_origen[2:]
                    # Sacamos el usuario y el puerto de origen.
                    user_origin[1] = [email_origen]
                    port_origen = int(user_list[email_origen][1])
                    ip_origen = user_list[email_origen][0]
                    # Buscamos el email destino en la lista de usuarios.
                    if email_destination not in user_list:
                        # Enviamos de vuelta al cliente de origen que no está
                        message = "SIP/2.0 User Not Found\r\n"
                        write_log(message, "Error", ip_origen, port_origen,
                                  log_path)
                        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                                as my_socket:
                            my_socket.setsockopt(socket.SOL_SOCKET,
                                                 socket.SO_REUSEADDR, 1)
                            my_socket.connect((ip_origen, int(port_origen)))
                            my_socket.send(bytes("SIP/2.0 User Not Found\r\n",
                                                 'utf-8'))
                    # El email está en la lista
                    else:
                        port_destination = int(user_list[email_destination][1])
                        ip_destination = user_list[email_destination][0]
                        write_log(line, "Received from",
                                  ip_origen, port_origen, log_path)
                        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                                as my_socket:
                            my_socket.setsockopt(socket.SOL_SOCKET,
                                                 socket.SO_REUSEADDR, 1)
                            my_socket.connect((ip_destination,
                                               int(port_destination)))
                            write_log(line, "Sent to", ip_destination,
                                      port_destination, log_path)
                            my_socket.send(bytes(line, 'utf-8'))
                            data = my_socket.recv(1024)
                            mes = data.decode('utf-8')

                        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                                as my_socket:
                            my_socket.setsockopt(socket.SOL_SOCKET,
                                                 socket.SO_REUSEADDR, 1)
                            my_socket.connect((ip_origen, int(port_origen)))
                            data = data.decode('utf-8')
                            my_socket.send(bytes(data, 'utf-8'))

                if line[0:7] == "SIP/2.0":
                    email_origen = user_origin[1][0]
                    port_destination = int(user_list[email_origen][1])
                    ip_destination = user_list[email_origen][0]
                    write_log(line, "Received from", self.client_address[0],
                              port_destination, log_path)
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                            as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET,
                                             socket.SO_REUSEADDR, 1)
                        my_socket.connect((ip_destination,
                                           int(port_destination)))
                        my_socket.send(bytes(line, 'utf-8'))
                        write_log(line, "Sent to", ip_destination,
                                  int(port_destination), log_path)
                if line[0:8] == "ACK sip:" or line[0:2] == "BYE":
                    email_destination = information[0].split()[-2]
                    email_destination = email_destination.split(":")[-1]
                    port_destination = int(user_list[email_destination][1])
                    ip_destination = user_list[email_destination][0]
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                            as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET,
                                             socket.SO_REUSEADDR, 1)
                        my_socket.connect((ip_destination,
                                           int(port_destination)))
                        my_socket.send(bytes(line, 'utf-8'))
                if line[0:14] == "SIP/2.0 200 OK":
                    email_origen = user_origin[1][0]
                    port_destination = int(user_list[email_origen][1])
                    ip_destination = user_list[email_origen][0]
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                            as my_socket:
                        my_socket.setsockopt(socket.SOL_SOCKET,
                                             socket.SO_REUSEADDR, 1)
                        my_socket.connect((ip_destination,
                                           int(port_destination)))
                        my_socket.send(bytes(line, 'utf-8'))
                        write_log(line, "Sent to", ip_destination,
                                  port_destination, log_path)
            else:
                break

    def register2json(self):
        """Registramos los valores """
        # Escribimos el diccionario en el fichero .json
        with open(users_file, "w") as outfile:
            json.dump(user_list, outfile, indent=1)

    def json2register(self):
        """register dictionary"""
        username = ""
        try:
            if path.exits(users_file):
                with open(users_file, "r") as data_file:
                    user_list[username] = json.load(data_file)
        except Exception:
            pass


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 proxy_registrar.py config")

    parser = make_parser()
    XMLClass = XMLHandlerProxy()
    parser.setContentHandler(XMLClass)
    parser.parse(open(str(sys.argv[1])))
    attributes = XMLClass.get_tags()
    # SERVER
    att = attributes[0]
    name_proxy = att["name"]
    ip_proxy = att["ip"]
    port_proxy = int(att["puerto"])
    # DATABASE
    att = attributes[1]
    users_file = att["path"]
    passwd_file = att["passwdpath"]
    # LOG
    att = attributes[2]
    log_path = att["path"]
    serv = socketserver.UDPServer(('', port_proxy), SIPRegisterHandler)
    # La lista de usuarios
    user_list = {}

    user_origin = {}
    starting = "Server " + name_proxy +\
               " listening at port " + str(port_proxy) + "..."
    write_log(starting, "Starting",
              ip_proxy, port_proxy, log_path)
    print(starting)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Server Down")
        write_log("Server OFF", "Finish",
                  ip_proxy, port_proxy, log_path)
